import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: [],
  loader: true,
};

const booksSlice = createSlice({
  name: "books",
  initialState,
  reducers: {
    getBooks: (state, action) => {
      state.data = action.payload;
    },
    setLoader: (state, action) => {
      state.loader = action.payload;
    },
  },
});

export const { getBooks, setLoader } = booksSlice.actions;

export default booksSlice.reducer;
