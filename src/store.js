import { configureStore } from "@reduxjs/toolkit";
import booksSlice from "./slices/bookSlice";

const store = configureStore({
    reducer : {
        booksData : booksSlice,
    }
});


export default store;