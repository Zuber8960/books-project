import React from "react";
import Navbar from "./components/Navbar";
import Books from "./components/Books";

const App = () => {
  return (
    <>
      <Navbar />
      <Books />
    </>
  );
};

export default App;
