import React, { useState } from "react";
import { useSelector } from "react-redux";
import Card from "./Card";
import { Grid, Pagination, CircularProgress } from "@mui/material";

const Books = () => {
  const booksData = useSelector((state) => state.booksData.data);
  const loading = useSelector((state) => state.booksData.loader);

  const itemsPerPage = 6;
  const [currentPage, setCurrentPage] = useState(1);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;

  const booksForCurrentPage = booksData.slice(startIndex, endIndex);

  const handlePageChange = (event, newPage) => {
    setCurrentPage(newPage);
  };


  return (
    <>
      {(loading && <CircularProgress />) || (
        <div>
          <Grid container spacing={2} sx={{marginTop : "10px"}}>
            {(booksForCurrentPage.length &&
              booksForCurrentPage.map((book) => (
                
                <Grid item xs={12} sm={6} md={4} key={book.id}>
                  <Card
                    title={book.title}
                    authors={book.authors}
                    download_count={book.download_count}
                  />
                </Grid>
              ))) ||
              ""}
          </Grid>
          <Pagination
            count={Math.ceil(booksData.length / itemsPerPage)}
            page={currentPage}
            onChange={handlePageChange}
            color="primary"
            style={{ marginTop: "20px" }}
          />
        </div>
      )}
    </>
  );
};

export default Books;
