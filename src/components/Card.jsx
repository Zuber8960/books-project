import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Button } from "@mui/material";

export default function MediaCard({ title, authors, download_count }) {
  const author = authors[0];
  const formattedName = author.name.split(",").join(" ");
  const birthYear = author.birth_year || "not found";
  const deathYear = author.death_year || "not found";

  const handleDownloadClick = () => {
    const contentToDownload = `
      Book: ${title}
      Downloads: ${download_count}
      Author details:
      name: ${formattedName}
      Birth: ${birthYear}
      Death: ${deathYear}
    `;

    const blob = new Blob([contentToDownload], { type: "text/plain" });

    const url = URL.createObjectURL(blob);

    const anchor = document.createElement("a");
    anchor.href = url;
    anchor.download = "author_details.txt";

    anchor.click();
    URL.revokeObjectURL(url);
  };

  //   console.log(title, authors, download_count);
  return (
    <Card sx={{ maxWidth: 345, maxHeight: 345, minHeight: 200 }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Book: {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Downloads: {download_count}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Author details:
        </Typography>
        <Typography variant="body2" color="text.secondary">
          name: {authors[0].name.split(",").join(" ")}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Birth: {authors[0].birth_year || "not found"}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Death: {authors[0].death_year || "not found"}
        </Typography>
      </CardContent>
      <Button onClick={handleDownloadClick} variant="outlined" color="primary">
        Download Details
      </Button>
    </Card>
  );
}
